// class definition.
var Price = function(price) {
	this.price = price;
	
	// @return taxed price.
	this.taxedPrice = function(tax) {
		var taxed = Math.round(price * (1 + tax/100));
		return new Price(taxed);
	}
	
	// @return formatted price.
	this.format = function() {
	    var asStr = new String(this.price);
	    var ret = '';
	    var interval = 3;
	    
	    while(asStr.length > interval) {
	        part = asStr.substring(asStr.length - interval, asStr.length);
	        ret = ',' + part + ret;
	        
	        asStr = asStr.substring(0, asStr.length - interval);
	    }
	    
	    ret = asStr + ret;
	    
	    return ret;
	}
}


// main code.
var price = new Price(1280).taxedPrice(5);
var textNode = document.createTextNode(price.format());
document.getElementById('taxedPrice').appendChild(textNode);
