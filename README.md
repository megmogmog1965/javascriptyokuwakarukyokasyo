## Description

[よくわかるJavaScriptの教科書](http://www.amazon.co.jp/gp/product/B00CALAWC8?btkr=1) のサンプルコードを実際に書いて動かしてみるRepositoryです

> 全部は書かない。どうでもいい所は飛ばす

## Requirement

* IE7
* FireFox
* Goole Chrome
* Safari

## References

* [よくわかるJavaScriptの教科書](http://www.amazon.co.jp/gp/product/B00CALAWC8?btkr=1)
* [jQuery]
* [jQuery日本語リファレンス]
* [Eclipse]

## Author

[Yusuke Kawatsu](https://bitbucket.org/megmogmog1965)
[jQuery]:http://jquery.com/
[jQuery日本語リファレンス]:http://semooh.jp/jquery/
[Eclipse]:https://eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/lunasr1
